import { Meteor } from 'meteor/meteor';
import Events from './collection';

Meteor.publish(Events.publication, function () {
  return Events.mongo.find({
    $or: [{ _idUser: this.userId }, { _idUser: { $exists: false } }],
    timestamp: { $gte: +new Date() },
  });
});
