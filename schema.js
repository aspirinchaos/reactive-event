import SimpleSchema from 'simpl-schema';
import { CoreSchema } from 'meteor/core';

/**
 * Схема серверных событий
 * @memberOf module:reactive-event
 */
const EventSchema = new SimpleSchema({
  name: {
    type: String,
    label: 'Наименование',
  },
  place: {
    type: String,
    label: 'Приложение',
  },
  data: {
    type: Object,
    blackbox: true,
    label: 'Данные события',
  },
  _idUser: {
    type: String,
    label: 'Событие связанное с пользователем',
    optional: true,
  },
  timestamp: {
    type: Number,
    label: 'Время возникновения',
  },
}).extend(CoreSchema);

export default EventSchema;
