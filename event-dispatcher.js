import { Meteor } from 'meteor/meteor';
import Events from './collection';
import ReactiveEvent from './reactive-event';
import ReactiveEventHandler from './reactive-event-handler';
import { dispatchEvent } from './methods';

/**
 * Класс диспетчера событий
 * Используется двумя синглтона, на клиенте и на сервере
 * @memberOf module:reactive-event
 */
class ReactiveEventDispatcher {
  /**
   * Хранение событий
   * @type {Object}
   */
  events = {};

  /**
   * Хранение обработчиков событий
   * @type {{all: Array}}
   */
  handlers = {
    all: [],
  };

  constructor() {
    if (!Meteor.isServer) {
      Meteor.subscribe(Events.publication);
      Events.mongo.find().observe({
        added: (event) => {
          // события которые приходят с сервера будут выполняться на клиенте
          // и иметь where='server', события возникающие на сервере и воздействующие на клиент не
          // могут пересекаться
          this.events[event.name].dispatch(event.data);
        },
      });
    }
  }

  /**
   * Регистрация события в диспетчере
   * @param {String} name - имя события
   * @param {module:reactive-event.ReactiveEvent} event - инстанс события
   */
  registerEvent(name, event) {
    if (!(event instanceof ReactiveEvent)) {
      throw new Meteor.Error('wrong-event-type', `Событие ${name} должно быть наследовано от ReactiveEvent`);
    }
    if (Object.hasOwnProperty.call(this.events, name)) {
      throw new Meteor.Error('duplicate-event', `Событие ${name} уже зарегистрировано, используйте другое имя`);
    }
    this.events[name] = event;
    this.handlers[name] = [];
  }

  /**
   * Регистрация обработчиков для событий
   * @param {Array<String>} events - массив имен событий для привязки обработчиков
   * @param {module:reactive-event.ReactiveEventHandler} handler - инстанс обработчика
   */
  registerHandler(events, handler) {
    if (!Array.isArray(events)) {
      throw new Meteor.Error('wrong-events-name', `Обработчик для событий ${events.join(', ')} должен содержать массив событий`);
    }
    if (!(handler instanceof ReactiveEventHandler)) {
      throw new Meteor.Error('wrong-handler-type', `Обработчик для событий ${events.join(', ')} должен быть наследован от ReactiveEventHandler`);
    }
    events.forEach((event) => {
      if (!Object.hasOwnProperty.call(this.events, event)) {
        throw new Meteor.Error('event-does-not-exist', `Событие ${event} не зарегистрировано, для работы обработчика необходимо сначала создать событие`);
      }
      this.handlers[event].push(handler);
    });
  }

  /**
   * Обработка вызова события
   * выполняем все привязанные обработчики
   * @param {module:reactive-event.ReactiveEvent} event - всплывшее событие
   */
  dispatchEvent(event) {
    if (!Meteor.isServer && !event.isServer) {
      dispatchEvent.callPromise(event);
    }
    if (!Object.hasOwnProperty.call(this.events, event.name)) {
      throw new Meteor.Error('event-does-not-exist', 'Что бы вызывать событие нужно его добавить с помощью addEvent!');
    }

    this.handlers.all.forEach(handler => handler.run(event));
    this.handlers[event.name].forEach(handler => handler.run(event));
    // Серверное событие и серверный диспетчер
    if (event.isServer && Meteor.isServer) {
      // сохраняем в бд серверное событие, преобразовав его в объект
      // монго позволяет использовать кастомные типы только для вложенных полей
      // а не для документов в целом
      Events.mongo.insert(event.toJSONValue());
    }
  }
}

const Dispatcher = new ReactiveEventDispatcher();

export default Dispatcher;
