import { Meteor } from 'meteor/meteor';
import { ValidatedMethod } from 'meteor/validated-method';
import { modulename } from './collection';
import Dispatcher from './event-dispatcher';
import ReactiveEvent from './reactive-event';

/**
 * Метод для общения с диспетчерами
 * @memberOf module:reactive-event
 * @type {ValidatedMethod}
 */
const dispatchEvent = new ValidatedMethod({
  name: `${modulename}.dispatch-event`,
  validate(event) {
    if (!(event instanceof ReactiveEvent)) {
      throw new Meteor.Error('wrong-event-type', 'Событие должно быть наследовано от ReactiveEvent');
    }
  },
  run(event) {
    // Если авторизованный пользователь
    if (this.userId && !event._idUser) {
      event._idUser = this.userId;
    }
    if (Meteor.isServer) {
      Dispatcher.dispatchEvent(event);
    }
  },
});

export { dispatchEvent };
