import { Meteor } from 'meteor/meteor';
import Dispatcher from './event-dispatcher';

/**
 * Класс для обработчиков событий
 * @memberOf module:reactive-event
 */
class ReactiveEventHandler {
  /**
   * Для каких событий этот обработчик
   * @type {Array}
   * @private
   */
  _events = [];

  /**
   * @param {String|Array} events - Имя события или массив имен событий
   * @param {Function} run - Функция обработки события
   */
  constructor({ events, run }) {
    if (!Array.isArray(events)) {
      events = [events];
    }
    if (!Array.length) {
      throw new Meteor.Error('events-empty', 'Для работы обработчика необходимо указать список событий');
    }
    if (typeof run !== 'function') {
      throw new Meteor.Error('run-is-not-function', 'Обработчик "run" должен быть функцией!');
    }
    this._events = events;
    this.run = run.bind(this);
    Dispatcher.registerHandler(events, this);
  }

  /**
   * Метод для обработки события, переопределяется в constructor
   * Должен быть обязательно переопределен
   * @override
   * @param {module:reactive-event.ReactiveEvent} event - всплывшее событие
   */
  run(event) {
    console.log(event);
  }

  /**
   * Динамическое создание инстанса
   * Если файл обработчика находится в директории глубже чем
   * @param handler
   * @return {module:reactive-event.ReactiveEventHandler}
   */
  static attach(handler) {
    return new ReactiveEventHandler(handler);
  }
}

export default ReactiveEventHandler;
