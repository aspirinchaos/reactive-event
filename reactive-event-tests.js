// Import Tinytest from the tinytest Meteor package.
import { Tinytest } from "meteor/tinytest";

// Import and rename a variable exported by reactive-event.js.
import { name as packageName } from "meteor/reactive-event";

// Write your tests here!
// Here is an example.
Tinytest.add('reactive-event - example', function (test) {
  test.equal(packageName, "reactive-event");
});
