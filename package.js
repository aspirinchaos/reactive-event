Package.describe({
  name: 'reactive-event',
  version: '0.1.3',
  summary: 'Диспетчер событий для метеор приложений на blaze',
  git: 'https://bitbucket.org/aspirinchaos/reactive-event.git',
  documentation: 'README.md',
});

/**
 * @module reactive-event
 */

Package.onUse((api) => {
  api.versionsFrom('1.5');
  api.use(['ecmascript', 'tmeasday:check-npm-versions', 'validated-method', 'core']);
  api.addFiles('publications.js', 'server');
  api.mainModule('main.js');
});

Package.onTest((api) => {
  api.use('ecmascript');
  api.use('tinytest');
  api.use('reactive-event');
  api.mainModule('reactive-event-tests.js');
});
