import { Document, Collection } from 'meteor/core';
import Schema from './schema.js';

const modulename = 'Event';

/**
 * Класс документа для серверного события
 * @memberOf module:reactive-event
 * @extends {module:core.Document}
 */
class Event extends Document {

}

/**
 * Коллекция для серверных событий
 * @memberOf module:reactive-event
 * @type {module:core.Collection}
 */
const Events = new Collection('Reactive_Event', Event, Schema);

/**
 * Публикация для события
 * @memberOf module:reactive-event.Events
 * @type {String}
 */
Events.publication = `${modulename}.observer`;


export { Events as default, modulename };
