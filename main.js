import { checkNpmVersions } from 'meteor/tmeasday:check-npm-versions';
import ReactiveEvent from './reactive-event';
import ReactiveEventHandler from './reactive-event-handler';

checkNpmVersions({
  'simpl-schema': '1.5.3',
}, 'reactive-event');

export { ReactiveEvent, ReactiveEventHandler };
