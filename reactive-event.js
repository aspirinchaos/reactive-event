import { Meteor } from 'meteor/meteor';
import { Tracker } from 'meteor/tracker';
import { EJSON } from 'meteor/ejson';
import Dispatcher from './event-dispatcher';

/**
 * Класс для событий
 * @memberOf module:reactive-event
 */
class ReactiveEvent {
  /**
   * Имя события
   * @type {String}
   */
  name = '';

  /**
   * Сторона всплытия события
   * @type {'client'|'server'|'cordova'}
   */
  where = 'client';

  /**
   * Приложение где событие всплыло
   * @type {String}
   */
  place = 'app';

  /**
   * Обработатывать событие или нет
   * @type {Boolean}
   */
  active = true;

  /**
   * Зависимость трекера
   * @where client
   * @type {Tracker.Dependency}
   * @private
   */
  _dep;

  /**
   * Время возникновения события
   * @type {Number}
   */
  timestamp = 0;

  /**
   * Данные переданные при вызове события
   * @type {Object}
   */
  data = {};

  /**
   * Сборка из EJSON отдельным параметром нужна, потому в методах создается новый инстанс
   * события, а событие должно быть синглотоном
   * @param {?Object} data - объект события
   * @param {String|Object} data.name - имя события или объект события
   * @param {String} data.where - сторона где событие создалось
   * @param {String} data.place - приложение где событие создалось
   * @param {Boolean} data.active - флаг обработки события
   * @param {Object} [json] - данные события из EJSON (выполняется в методах)
   */
  constructor(data, json) {
    if (json && typeof json === 'object') {
      Object.assign(this, json);
    } else {
      const {
        name, where, place = 'app', active = true,
      } = data;
      if (!name) {
        throw new Meteor.Error('event-name-empty', 'Имя события не может быть пустым');
      }
      this.name = name;
      if (!where) {
        throw new Meteor.Error('event-where-empty', 'Сторона выполнения события не может быть пустым');
      }
      this.where = where;
      if (!Meteor.isServer) {
        this._dep = new Tracker.Dependency();
      }
      this.place = place;
      this.active = active;
      Dispatcher.registerEvent(name, this);
    }
  }

  /**
   * Проверка расположения события
   * @return {Boolean}
   */
  get isServer() {
    return this.where === 'server';
  }

  /**
   * Всплытие события
   * @param {Object} data - данные события
   * @param {String} _idUser - ид пользователя
   */
  dispatch(data = {}, _idUser = '') {
    this.data = data;
    this.timestamp = +new Date();
    if (_idUser) {
      this._idUser = _idUser;
    }
    if (!this.active) {
      return;
    }
    Dispatcher.dispatchEvent(this);
    if (!Meteor.isServer) {
      this._dep.changed();
    }
  }

  /**
   * Подписка на всплытие события
   * @return {Promise}
   */
  subscribe() {
    if (!Meteor.isServer && Tracker.active) {
      this._dep.depend();
    }
    // событие еще не было вызвано
    if (!this.timestamp) {
      // Возвращаем не выполняемый промис для того, чтобы:
      // 1. не выполнился сразу после подписки
      // 2. что бы не было ошибки при вызове then
      return new Promise(() => {
      });
    }
    return Promise.resolve(this);
  }

  /**
   * Включение события
   */
  activate() {
    this.active = true;
  }

  /**
   * Выключение события
   */
  deactivate() {
    this.active = false;
  }

  /**
   * Для работы с EJSON
   * @return {Object}
   */
  toJSONValue() {
    const {
      name, where, place, timestamp, data, _idUser = '',
    } = this;
    return {
      name, where, place, timestamp, data, _idUser,
    };
  }

  /**
   * Для работы с EJSON
   * @return {String}
   */
  typeName() {
    return 'ReactiveEvent';
  }
}

EJSON.addType('ReactiveEvent', json => new ReactiveEvent(null, json));

export default ReactiveEvent;
